package apps

/**
  * Created by mac024 on 2017/4/17.
  */
object FractionApp extends App{
val frac=cc.Fraction(1,2)

  println(frac)
}



object FractionApp1 extends App{
  val frac1=cc.Fraction(3)

  println(frac1)
}

//
object FractionApp2 extends App{
  val frac2=cc.Fraction(3,6)
  println(frac2.reduce)
}


object FractionApp3 extends App{
  val frac3=cc.Fraction(1,2)
  val frac4=cc.Fraction(1,3)

  val frac5=frac3 plus frac4
  println(frac5)

}
